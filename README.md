# P-Cart PostgreSQL

This provides PostgreSQL 9.3 for production deployment.

* `docker build -t pcart-db .`
* `docker run pcart-db`

Ports

* 5432

Environment Variables

* `DB_USER`: Database user (default: pcart)
* `DB_PASS`: Database pass (default: \<randomly generated\>)
* `DB_NAME`: Database name (default: pcart)

Based on: https://github.com/shipyard/docker-shipyard-db
